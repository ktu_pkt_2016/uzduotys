%number_Counter([m,m,1,8,9,x,2,z,4,4,m,1],0).
number_Counter([], Sum) :-
	write(Sum).
	
number_Counter([Head|Tail], Sum) :-
	( 
    integer(Head) ->
	X is Sum+1,
	number_Counter(Tail, X)
	;
	number_Counter(Tail, Sum) 
    ).
