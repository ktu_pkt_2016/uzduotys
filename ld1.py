﻿ # uzduotis 10494 - If We Were a Child Again
 # http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=16&page=show_problem&problem=1435

class Storage:

    def __init__(self, num1, num2, sign):
        self.Num1 = num1
        self.Num2 = num2
        self.Sign = sign

    def DoTheSuperMagic(self):
        if (self.Sign == '/'):
            self.Result = self.Num1 // self.Num2
        else:
            self.Result = self.Num1 % self.Num2

def ReadData(filename):
    items = []

    allLines = open(filename)
    for singleLine in allLines:
        line = singleLine.strip()
        strings = line.split()
        items.append(Storage(int(strings[0]), int(strings[2]), strings[1]))

    return items

def PrintData(items):
    print("Given data")

    for item in items:
        print(str(item.Num1) + " " + item.Sign + " " + str(item.Num2))

    print()
    
def DoTheMagic(items):
    for item in items:
        item.DoTheSuperMagic()

def PrintResults(items):
    print("Results")
    for item in items:
        print(item.Result)

def main():
    storedItems = ReadData("data.txt")
    PrintData(storedItems)
    DoTheMagic(storedItems)
    PrintResults(storedItems)

main()