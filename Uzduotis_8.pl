
initiate(Listas) :-
	(
	flatten(Listas,L),
	number_Counter(L,0)
	).
	
	
number_Counter([],Sum) :-
	write('Saraso elementu suma: '), write(Sum),nl.
	
	
number_Counter([Head|Tail], Sum) :-
	( 
    integer(Head) ->
	X is Sum+Head,
	number_Counter(Tail, X)
	;
	number_Counter(Tail, Sum) 
    ).

