-- Laurynas Tamosaitis IFF 3-1
-- ghci ld3.hs
import Data.List
import System.IO

task1 :: Double -> Double -> Double -> Double               
task1 a b c = (a + b + c)

briaunos :: Double -> Double -> Double
briaunos a b = (a + b)

getCosinus :: Double -> Double -> Double -> Double
getCosinus a b c = (((a*a) - (b*b) - (c*c))/(2*b*c*(-1)))

triangleArea :: Double -> Double -> Double -> Double
triangleArea b c sinA = ((b)*(c)*(sinA))/2

circleAreas :: Double -> Double -> Double
circleAreas r rad = ((r)*(r)*rad)/2

main = do
	putStrLn "Rasykite spindulius... "
	input1 <- readLn
	input2 <- readLn
	input3 <- readLn
	putStrLn $ "Trikampių kraštinės..."
	let b1 = briaunos(input1 :: Double) (input2 :: Double)
	let b2 = briaunos(input2 :: Double) (input3 :: Double)
	let b3 = briaunos(input1 :: Double) (input3 :: Double)
	putStrLn $ "Ats.: " ++ show b1 ++ "   " ++ show b2 ++ "   " ++ show b3
	putStrLn $ "Kosinusai..."
	let c1 = getCosinus(b3 :: Double) (b2 :: Double) (b1 :: Double)
	let r1 = input2
	let c2 = getCosinus(b2 :: Double) (b1 :: Double)(b3 :: Double)
	let r2 = input1
	let c3 = getCosinus(b1 :: Double) (b3 :: Double) (b2 :: Double)
	let r3 = input3
	putStrLn $ "Ats.: " ++ show c1  ++ "   " ++ show c2 ++ "   " ++ show c3
	
	------------
	putStrLn $ "Kampu radianais..."
	let rad1 = acos c1
	let rad2 = acos c2
	let rad3 = acos c3
	putStrLn $ "Kampai: " ++ show rad1 ++ "   " ++ show rad2 ++ "   " ++ show rad3
	
	--- Trikampio plotas
	let sinA = sin rad1
	let areaOfTriangle = triangleArea b1 b2 sinA
	putStrLn $ "Trikampio plotas..."
	putStrLn $ "Trikampio plotas: " ++ show areaOfTriangle
	
	--- Apskritimu plotai
	let area1 = circleAreas r1 rad1
	let area2 = circleAreas r2 rad2
	let area3 = circleAreas r3 rad3
	putStrLn $ "in 1 " ++ show r1 ++ "  rad1  " ++ show rad1
	putStrLn $ "in 2 " ++ show r2 ++ "  rad2  " ++ show rad2
	putStrLn $ "in 3 " ++ show r3 ++ "  rad3  " ++ show rad3
	putStrLn $ "Apskritimu plotai..."
	putStrLn $ "Apskritimo plotai: " ++ show area1 ++ "   " ++ show area2 ++ "   " ++ show area3
	---Final
	let ats = areaOfTriangle - area1 - area2 - area3
	putStrLn $ "Atsakymas:               >>>>>   "  ++ show ats ++ "   <<<<< "
	
	


	
	
