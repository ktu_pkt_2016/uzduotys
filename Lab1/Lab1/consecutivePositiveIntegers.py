from math import sqrt

class consecutivePositiveIntegers(object):
    def __init__(self, fileName):
        with open(fileName, "r") as ins:
            self.arInput = []
            self.arOutout = []
            for line in ins:
                self.arInput.append(int(line.strip('\n')));
    def printInput(self):
        for el in self.arInput:
            print(el)
    def printOutput(self):
        for el in self.arOutout:
            print(el)
    def calculateOutputFaster(self):
        for val in self.arInput:
            self.arOutout.append(1)
            start = int(val/2+0.5)
            end = int((-1+sqrt(1+8*val))/2)
            
            step = start - 1
            stepLength = 2
            sum = start + step
            for i in range(start, end, -1):
                while sum < val:
                    step -= 1
                    stepLength += 1
                    sum += step
                if sum == val:
                    self.arOutout[-1] += 1
                elif sum > val:
                    step += 1
                    stepLength -= 1
                    sum -= step
                
                sum -= stepLength

    def calculateOutput(self):
        for val in self.arInput:
            self.arOutout.append(1)
            start = int(val/2+0.5)
            end = int((-1+sqrt(1+8*val))/2)
            for i in range(start, end, -1):
                st = i - 1
                sk = i + st
                string = str(i)+" " + str(st)+" " 
                while sk < val:
                    st -= 1
                    sk += st
                    string
                    string += str(st)+ " "
                if sk == val:
                    self.arOutout[-1] += 1
                    print(string)