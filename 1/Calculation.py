import math

class Calculation:
    def __init__(self, name, r1, r2, r3):
        self.name = name
        self.r1 = r1
        self.r2 = r2
        self.r3 = r3
        self.Pi = 3.141592653589

    def FindAnglesInRadians(self):
        alfaLeft = math.pow(self.a, 2) - math.pow(self.b, 2) - (math.pow(self.c, 2))
        alfaRight = 2 * self.b * self.c * (-1)
        self.alfa = math.acos(alfaLeft/alfaRight)

        betaLeft = math.pow(self.b, 2) - math.pow(self.a, 2) - math.pow(self.c, 2)
        betaRight = 2 * self.a * self.c * (-1)
        self.beta = math.acos(betaLeft / betaRight)

        gamaLeft = math.pow(self.c, 2) - math.pow(self.a, 2) - math.pow(self.b, 2)
        gamaRight = 2 * self.a * self.b * (-1)
        self.gama = math.acos(gamaLeft/gamaRight)

        print("  KAMPAI:     >>>> " + str(self.alfa) + "   " + str(self.beta) + "   " + str(self.gama))

    def FindAreas(self):
        self.area1 = 0.5 * self.alfa * (self.r3*self.r3)
        self.area2 = 0.5 * self.beta * (self.r1*self.r1)
        self.area3 = 0.5 * self.gama * (self.r2*self.r2)
        print("  Apskritimu dengiamos vietos: " + str(self.area1) + "  " + str(self.area2) + "  " + str(self.area3))


    def TeritoryOfTriangle(self):
        self.a = self.r1 + self.r2
        self.b = self.r2 + self.r3
        self.c = self.r3 + self.r1
        self.P = (self.a + self.b + self.c)/2
        print("*****************    " + self.name + "    *****************")
        print(self.name + " I-a krastine (a) " + str(self.a) + " II-a krastine (b) " + str(self.b)
              + " III-a krastine (c) " + str(self.c)  )
        print("  pusperimetris  " + str(self.P))
        self.area = math.sqrt(self.P * (self.P - self.a) * (self.P - self.b) * (self.P - self.c))
        print("  Trikampio plotas: " + str(self.area))

    def Start(self):
        self.TeritoryOfTriangle()
        self.FindAnglesInRadians()
        self.FindAreas()
        self.plotas = round ((self.area - self.area1 - self.area2 - self.area3), 4)
        print("ATSAKYMAS: " + str(self.plotas))
        print("\n")