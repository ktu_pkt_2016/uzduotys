import Calculation

def readFile(name):
    # duomenu skaitymas
    listas=[]
    f = open(name)
    numberOfCases = int(f.read(1))
    f.seek(3)
    data = f.read()
    data = data + " " # Tam kad paimtu ir paskutini skaitmeni ( t.y. kaip zyma kad failo pabaiga )
    skai=''
    for i in data:
        if(i != ' ' and i != '\n'):
            skai = skai+i
        else:
            listas.append(float(skai))
            skai=''
    return listas




# main "f-ja"
print("Programa ploto radimas pradeda darbą ... ")
listas = readFile('Input.txt')
print("\n")
print(listas)
print("\n")
j=0
index = 1
objectListas=[]
while(j != listas.__len__()):
    j += 1
    if(j%3 == 0):
        name = "Skaiciavimas " + str(index)
        obj=Calculation.Calculation(name, listas[j-3], listas[j-2], listas[j-1])
        objectListas.append(obj)
        index += 1


for j in objectListas:
    j.Start()

print("Pabaiga")